// Deklarerar tomma variabler
var canvas = null, ctx = null, birdImg = null;

// Var bilden skall visas
var xPos = 260, yPos = 220;

// Lagrar tangenttryckningar
var keysDown = {};

init = function() {
    addEventListener("keydown", keyDown, false);
    addEventListener("keyup", keyUp, false);

    canvas = document.getElementById("gameCanvas");
    ctx = canvas.getContext("2d");

    // Ange måtten på ritytan 'canvas'
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    // För att ladda in bilden
    birdImg = new Image();
    birdImg.onload = gameLoop;
    birdImg.src = "twitter.png";
}

// Lagra tangenttryckning
function keyDown(e) {
    keysDown[e.keyCode] = true;
}

// Ta bort tangenttryckning
function keyUp(e) {
    delete keysDown[e.keyCode];
}

// Spelloopen
function gameLoop() {
    // Om vi tryckt vänsterpil
    if (37 in keysDown) {
        xPos -= 5;
    }
    // Om vi tryckt högerpil
    if (39 in keysDown) {
        xPos += 5;
    }
    // Om vi tryckt uppåtpil
    if (38 in keysDown) {
        yPos -= 5;
    }
    // Om vi tryckt nedåtpil
    if (40 in keysDown) {
        yPos += 5;
    }

    ctx.save();
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(birdImg, xPos, yPos, 50, 50);
    ctx.restore();

    requestAnimationFrame(function() {
        gameLoop();
    })
}

// Kör igång när sidan har laddats klart
window.addEventListener("load", init, false);
