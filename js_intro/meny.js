function slumpa() {
    var rad = "";
    var slump;

    /* En lista maträtter */
    var matRatter = ["Kebab", "Pasta carbonara", "Pasta bolognese", "Pizza", "Sushi", "Ärtsoppa", "Köttbullar och potatis", "Kinamat", "Lasagne", "Grekisk sallad", "Blodpudding", "Kallops", "Kroppskakor"];

    /* Vi skall slumpa 5 rätter */
    for (i = 1; i <= 5; i++) {

        /* Slump är 0, 1, 2, 3, 4 .. 12 */
        slump = Math.floor(Math.random() * 13);
        console.log(slump);

        rad = rad + "<td>" + matRatter[slump] + "</td>";
        console.log(rad);
    }

    /* Skjut in ny text i tabellen */
    document.getElementById("mat").innerHTML = rad;
}

function slumpaBg() {
    /* Slumpa fram ett tal: 0, 1, 2, 3, 4 */
    var slump = Math.floor(Math.random() * 5);
    var farg;

    switch (slump) {
        case 0:
            farg = "red";
            break;
        case 1:
            farg = "green";
            break;
        case 2:
            farg = "brown";
            break;
        case 3:
            farg = "yellow";
            break;
        case 4:
            farg = "pink";
            break;
    }

    /* Tillämpa färg på body */
    document.body.style.backgroundColor = farg;

    /* Kör funktionen var 100ms, dvs 10 ggr/sek */
    window.setTimeout(slumpaBg, 100);
}


