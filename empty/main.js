var mainState = {
    preload: function () {
        game.load.spritesheet('bird', 'assets/flappy_bird.png', 34, 24, 3);
        game.load.spritesheet('pipe', 'assets/flappy_pipes.png', 54, 320, 2);
    },
    create: function () {
        game.stage.backgroundColor = '#70ebf0';
        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.bird = game.add.sprite(100, 245, 'bird');
        this.bird.animations.add('fly', [0, 1, 2], 10, true);
        this.bird.animations.play('fly');

        game.physics.arcade.enable(this.bird);
        this.bird.body.gravity.y = 1000;

        var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.jump, this);

        this.timer = game.time.events.loop(1500, this.addRowOfPipes, this);
    },
    update: function () {
        if (this.bird.y < 0 || this.bird.y > game.world.width)
            this.restartGame();
    },
    jump: function () {
        this.bird.body.velocity.y = -350;
    },
    restartGame: function () {
        game.state.start('main');
    },
    addOnePipe: function (x, y) {
        var pipe = game.add.sprite(x, y, 'pipe');
        game.physics.arcade.enable(pipe);
        pipe.body.velocity.x = -200;
        pipe.checkWorldBounds = true;
        pipe.outOfBoundsKill = true;
    },
    addRowOfPipes: function () {
        var hole = Math.floor(Math.random() * 5) + 1;
        for (var i = 0; i < 8; i++)
            if (i != hole && i != hole + 1)
                this.addOnePipe(400, i * 60 + 10);
    },
};

var game = new Phaser.Game(400, 490);

game.state.add('main', mainState, true);
