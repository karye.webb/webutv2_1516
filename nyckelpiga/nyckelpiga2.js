// Allmäna variabler
var c, ctx;

// Figurens variabler
var ladybug, xPos, yPos, rotation = 0;

// Inställningar för banan
var mapIndexOffset = -1;
var mapRows = 15;
var mapCols = 15;
var tileMap = [
      [32, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 32]
    , [ 1,  1,  1,  1, 26,  1,  1,  1,  1, 26,  1,  1,  1,  1, 32]
    , [32,  1, 26,  1, 26,  1, 26, 26,  1, 26,  1, 26, 26,  1, 32]
    , [32, 26, 26,  1, 26,  1,  1, 26,  1, 26,  1,  1, 26,  1, 32]
    , [32,  1,  1,  1, 26, 26,  1, 26,  1, 26, 26,  1, 26,  1, 32]
    , [32,  1, 26, 26, 26,  1,  1, 26,  1, 26,  1,  1, 26,  1, 32]
    , [32,  1,  1,  1,  1,  1,  1, 26,  1, 26,  1,  1, 26,  1, 32]
    , [32, 26, 26, 26, 26, 26, 26, 26,  1, 26,  1, 26, 26,  1, 32]
    , [32,  1,  1,  1,  1,  1, 1 ,  1,  1, 26,  1, 26,  1,  1, 32]
    , [32,  1,  1,  1,  1,  1,  1,  1,  1, 26,  1, 26,  1,  1, 32]
    , [32,  1,  1, 26, 26, 26, 26, 26, 26, 26,  1, 26,  1,  1, 32]
    , [32,  1,  1, 26,  1,  1,  1, 26,  1,  1,  1, 26,  1,  1, 32]
    , [32,  1,  1,  1,  1, 26,  1,  1,  1, 26,  1, 26,  1,  1,  1]
    , [32,  1,  1,  1,  1, 26,  1,  1,  1, 26,  1, 26,  1,  1,  1]
    , [32, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 32]
   ];
var tileSheet = new Image();

function run() {
    c = document.getElementById("myCanvas");
    ctx = c.getContext("2d");

    // För att ladda in bilden
    ladybug = new Image();
    ladybug.src = "nyckelpiga.png";

    // Figurens ursprungsläge
    xPos = 1;
    yPos = 1;

    // Laddar in tilesheet-bilden med alla rutor
    tileSheet.src = "tanks_sheet.png";

    // Upprepa var 30ms
    window.setInterval(gameLoop, 30);
}

// Ritar banans alla rutor enligt arrayen
function drawLabyrint() {
    for (var rowCtr = 0; rowCtr < mapRows; rowCtr++) {
        for (var colCtr = 0; colCtr < mapCols; colCtr++) {
            var tileId = tileMap[rowCtr][colCtr] + mapIndexOffset;
            var sourceX = Math.floor(tileId % 8) * 32;
            var sourceY = Math.floor(tileId / 8) * 32;
            ctx.drawImage(tileSheet, sourceX,
                sourceY, 32, 32, colCtr * 32, rowCtr * 32, 32, 32);
        }
    }
}

function gameLoop() {
    // Sudda bort allt
    ctx.clearRect(0, 0, c.width, c.height);

    // Rita labyrintbanan
    drawLabyrint();

    // Rita ut spelaren
    ctx.save();
    ctx.translate(xPos * 32 + 16, yPos * 32 + 16);
    ctx.rotate(rotation);
    ctx.drawImage(ladybug, -16, -16, 32, 32);
    ctx.restore();
}

    // Lyssna på tangentnedtryckningar
function keyDown(e) {
    switch (e.keyCode) {
        case 37: // Vänster
            if (tileMap[yPos][xPos - 1] == 1)
                xPos--;
            rotation = 270*Math.PI/180;
            break;

        case 39: // Höger
            if (tileMap[yPos][xPos + 1] == 1)
                xPos++;
            rotation = 90*Math.PI/180;
            break;

        case 38: // Upp
            if (tileMap[yPos - 1][xPos] == 1)
                yPos--;
            rotation = 0;
            break;

        case 40: // Ned
            if (tileMap[yPos + 1][xPos] == 1)
                yPos++;
            rotation = Math.PI;
            break;
    }
}
